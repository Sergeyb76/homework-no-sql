﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDbSettings _settings;
        public EfDbInitializer(IMongoDbSettings settings)
        {
            this._settings = settings;
        }
        
        public void InitializeDb()
        {
            var client = new MongoClient(_settings.ConnectionString);
            client.DropDatabase(_settings.Database);
            var database = client.GetDatabase(_settings.Database);
            var employees = database.GetCollection<Employee>("Employees");
            var roles = database.GetCollection<Role>("Roles");
            employees.InsertMany(FakeDataFactory.Employees);
            roles.InsertMany(FakeDataFactory.Roles);
        }
    }
}