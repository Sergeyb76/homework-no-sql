﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public interface IMongoDbSettings
    {
        string ConnectionString { get; set; }
        string Database { get; set; }
    }
}
